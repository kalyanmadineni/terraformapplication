resource "aws_vpc" "public_vpc"{
    cidr_block="${var.vpc_cidr}"
    enable_dns_hostnames=true
    tags={
        name="${var.vpc_name}"
        Owner="${var.Owner}"
        environment="${var.environment}"
    }
}
resource "aws_subnet" "public_subnet1"{
    vpc_id="${aws_vpc.public_vpc.id}"
    cidr_block="${var.public_subnet1_cidr}"
    availability_zone="${var.public_subnet1_availablityzone}"
    map_public_ip_on_launch=true
    tags={
        name="${var.public_subnet1_name}"
        Owner="${var.Owner}"
        environment="${var.environment}"
    }
}
resource "aws_subnet" "public_subnet2"{
    vpc_id="${aws_vpc.public_vpc.id}"
    cidr_block="${var.public_subnet2_cidr}"
    availability_zone="${var.public_subnet2_availablityzone}"
    map_public_ip_on_launch=true
    tags={
        name="${var.public_subnet2_name}"
        Owner="${var.Owner}"
        environment="${var.environment}"
    }
}
resource "aws_subnet" "public_subnet3"{
    vpc_id="${aws_vpc.public_vpc.id}"
    cidr_block="${var.public_subnet3_cidr}"
    availability_zone="${var.public_subnet3_availablityzone}"
     map_public_ip_on_launch=true
    tags={
        name="${var.public_subnet3_name}"
        Owner="${var.Owner}"
        environment="${var.environment}"
    }
}
resource "aws_internet_gateway" "public_igw"{
    vpc_id="${aws_vpc.public_vpc.id}"
    tags={
        name="${var.public_igw_name}"
        Owner="${var.Owner}"
        environment="${var.environment}"
    }
}
resource "aws_route_table" "public_rt" {
    vpc_id="${aws_vpc.public_vpc.id}"
    route {
        cidr_block = "${var.route_public}"
        gateway_id = "${aws_internet_gateway.public_igw.id}"
    }
    tags = {
        Name = "${var.routetable_name}"
        Owner="${var.Owner}"
        environment="${var.environment}"
    }
}
resource "aws_route_table_association" "public_rt_subnet_association1" {
    subnet_id = "${aws_subnet.public_subnet1.id}"
    route_table_id = "${aws_route_table.public_rt.id}"
}
resource "aws_route_table_association" "public_rt_subnet_association2" {
    subnet_id = "${aws_subnet.public_subnet2.id}"
    route_table_id = "${aws_route_table.public_rt.id}"
}
resource "aws_route_table_association" "public_rt_subnet_association3" {
    subnet_id = "${aws_subnet.public_subnet3.id}"
    route_table_id = "${aws_route_table.public_rt.id}"
}


