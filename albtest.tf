
resource "aws_lb_target_group" "kalyan_tg" {
  name     = "kalyan-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.public_vpc.id
}
resource "aws_lb_target_group_attachment" "tg_instance_attachment" {
  target_group_arn = aws_lb_target_group.kalyan_tg.arn
  target_id        = aws_instance.testserver.id
  port             = 80
}
resource "aws_lb" "kalyan_alp" {
  name               = "kalyan-alp"
  internal           = false
  load_balancer_type = "application"
  security_groups    =[ "${aws_security_group.TestSG.id}"]
//  vpc_id             = "${aws_vpc.public_vpc.id}"
  subnets            =["${aws_subnet.public_subnet2.id}", "${aws_subnet.public_subnet1.id}"]
  enable_deletion_protection = true
  tags = {
    Environment = "test"
  }
}
