resource "aws_route53_zone" "primary" {
  name = "devopskalyanmgmail.com"
}
resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "devopskalyanmgmail.com"
  type    = "A"

  alias {
    name                   = "${aws_lb.kalyan_alp.dns_name}"
    zone_id                = "${aws_lb.kalyan_alp.zone_id}"
    evaluate_target_health = true
  }
}
