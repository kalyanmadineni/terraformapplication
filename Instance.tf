resource "aws_instance" "testserver" {
    ami = "${var.ec2_ami_id}"
    availability_zone = "${var.ec2_availability_zone}"
    instance_type = "${var.ec2_instance_type}"
    key_name = "${var.ec2_key_name}"
    //subnet_id="${var.ec2_subnet_id}"
    //vpc_security_group_ids=["${var.ec2_security_group}"]
    subnet_id = "${aws_subnet.public_subnet1.id}"
    vpc_security_group_ids =["${aws_vpc.public_vpc.default_security_group_id}"]
    associate_public_ip_address = true
    tags={
        name="test"
        Owner="${var.Owner}"
        environment="${var.environment}"
    }
}
resource "aws_eip" "elasticip" {
  instance = "${aws_instance.testserver.id}"
  tags={
        name="test"
        Owner="${var.Owner}"
        environment="${var.environment}"
    }
}

