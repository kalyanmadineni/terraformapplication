resource "aws_security_group" "TestSG" {
  name        = "TestSG"
  description = "TestingSG"
   vpc_id="${aws_vpc.public_vpc.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress{
      from_port = 0
      to_port   = 0
      protocol  = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    }
tags = {
        Name = "TestSG"
        Owner="${var.Owner}"
        environment="${var.environment}"
    }
}



